package com.squad4.trainingacademy.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyList;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.squad4.trainingacademy.dto.ApiResponse;
import com.squad4.trainingacademy.entity.Course;
import com.squad4.trainingacademy.entity.Enrollment;
import com.squad4.trainingacademy.entity.Status;
import com.squad4.trainingacademy.entity.TrainingCalender;
import com.squad4.trainingacademy.entity.User;
import com.squad4.trainingacademy.exception.CalenderIdNotFoundException;
import com.squad4.trainingacademy.exception.CourseAlreadyRequestedException;
import com.squad4.trainingacademy.exception.InvalidDateException;
import com.squad4.trainingacademy.exception.OnlyThreeCourseException;
import com.squad4.trainingacademy.repository.EnrollmentRepository;
import com.squad4.trainingacademy.repository.TrainingCalenderRepository;
import com.squad4.trainingacademy.repository.UserRepository;
import com.squad4.trainingacademy.serviceimpl.EnrollmentServiceImpl;
import com.squad4.trainingacademy.util.CustomCode;

@ExtendWith(SpringExtension.class)
class EnrollmentServiceImplTest {
	@Mock
	UserRepository userRepository;
	@Mock
	TrainingCalenderRepository calenderRepository;
	@Mock
	EnrollmentRepository enrollmentRepository;
	@InjectMocks
	EnrollmentServiceImpl enrollmentServiceImpl;
	
	@Test
	void testSuccess() {
		Long userId=1L;
		List<Long> calenderIds= new ArrayList<>();
		calenderIds.add(1L);
		Course course1=Course.builder().courseId(1L).build();
		Course course2=Course.builder().courseId(1L).build();
		User user=User.builder().userId(1L).build();
		TrainingCalender calender1=new TrainingCalender(1L, course1, "Vamsha", LocalDate.parse("2024-05-15"), LocalDate.parse("2024-05-25"));
		TrainingCalender calender2=new TrainingCalender(2L, course2, "Vamsha", LocalDate.parse("2024-05-15"), LocalDate.parse("2024-05-25"));
		Enrollment enrollment=Enrollment.builder().enrollmentId(1L).user(user).trainingCalender(calender2).staus(Status.INPROGRESS).build();
		Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));
		Mockito.when(calenderRepository.findAllById(calenderIds)).thenReturn(Arrays.asList(calender1));
		Mockito.when(enrollmentRepository.findByUser(user)).thenReturn(Arrays.asList(enrollment));
	    Mockito.when(enrollmentRepository.saveAll(anyList())).thenReturn(Arrays.asList(Enrollment.builder().enrollmentId(2L).build()));
		ApiResponse apiResponse=enrollmentServiceImpl.enrollCourse(userId, calenderIds);
		assertEquals(CustomCode.COURSE_ENROLLED_SUCCESSFULLY_MESSAGE, apiResponse.message());	
	}
	
	@Test
	void testCalenderIdNotFoundException() {
		Long userId=1L;
		List<Long> calenderIds= new ArrayList<>();
		calenderIds.add(1L);
		User user=User.builder().userId(1L).build();
		Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));
		Mockito.when(calenderRepository.findAllById(calenderIds)).thenReturn(Collections.emptyList());
		assertThrows(CalenderIdNotFoundException.class, ()->enrollmentServiceImpl.enrollCourse(userId, calenderIds));
	}
	
	@Test
	void testInvalidDateException() {
		Long userId=1L;
		List<Long> calenderIds= new ArrayList<>();
		calenderIds.add(1L);
		calenderIds.add(2L);
		calenderIds.add(3L);
		calenderIds.add(4L);
		calenderIds.add(5L);
		Course course1=Course.builder().courseId(1L).build();
		Course course2=Course.builder().courseId(1L).build();
		User user=User.builder().userId(1L).build();
		TrainingCalender calender1=new TrainingCalender(1L, course1, "Vamsha", LocalDate.parse("2024-05-08"), LocalDate.parse("2024-05-25"));
		TrainingCalender calender2=new TrainingCalender(2L, course2, "Vamsha", LocalDate.parse("2024-05-09"), LocalDate.parse("2024-05-25"));
		TrainingCalender calender3=new TrainingCalender(3L, course2, "Vamsha", LocalDate.parse("2024-05-10"), LocalDate.parse("2024-05-25"));
		TrainingCalender calender4=new TrainingCalender(4L, course2, "Vamsha", LocalDate.parse("2024-04-12"), LocalDate.parse("2024-05-25"));
		TrainingCalender calender5=new TrainingCalender(5L, course2, "Vamsha", LocalDate.parse("2024-06-12"), LocalDate.parse("2024-05-25"));
		Enrollment enrollment1=Enrollment.builder().enrollmentId(1L).user(user).trainingCalender(calender2).staus(Status.COMPLETED).build();
		Enrollment enrollment2=Enrollment.builder().enrollmentId(1L).user(user).trainingCalender(calender3).staus(Status.COMPLETED).build();
		Enrollment enrollment3=Enrollment.builder().enrollmentId(1L).user(user).trainingCalender(calender4).staus(Status.COMPLETED).build();
		Enrollment enrollment4=Enrollment.builder().enrollmentId(1L).user(user).trainingCalender(calender5).staus(Status.COMPLETED).build();
		Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));
		Mockito.when(calenderRepository.findAllById(calenderIds)).thenReturn(Arrays.asList(calender1));
		Mockito.when(enrollmentRepository.findByUser(user)).thenReturn(Arrays.asList(enrollment1,enrollment2,enrollment3,enrollment4));
		assertThrows(InvalidDateException.class, ()->enrollmentServiceImpl.enrollCourse(userId, calenderIds));
	}
	
	
	@Test
	void testCourseAlreadyRequestedException() {
		Long userId=1L;
		List<Long> calenderIds= new ArrayList<>();
		calenderIds.add(1L);
		Course course1=Course.builder().courseId(1L).build();
		Course course2=Course.builder().courseId(1L).build();
		User user=User.builder().userId(1L).build();
		TrainingCalender calender1=new TrainingCalender(1L, course1, "Vamsha", LocalDate.parse("2024-05-15"), LocalDate.parse("2024-05-25"));
		TrainingCalender calender2=new TrainingCalender(1L, course2, "Vamsha", LocalDate.parse("2024-05-15"), LocalDate.parse("2024-05-25"));
		Enrollment enrollment=Enrollment.builder().enrollmentId(1L).user(user).trainingCalender(calender2).staus(Status.INPROGRESS).build();
		Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));
		Mockito.when(calenderRepository.findAllById(calenderIds)).thenReturn(Arrays.asList(calender1));
		Mockito.when(enrollmentRepository.findByUser(user)).thenReturn(Arrays.asList(enrollment));
	    Mockito.when(enrollmentRepository.saveAll(anyList())).thenReturn(Arrays.asList(Enrollment.builder().enrollmentId(2L).build()));
		assertThrows(CourseAlreadyRequestedException.class, ()->enrollmentServiceImpl.enrollCourse(userId, calenderIds));	
	}
	
	
	@Test
	void testOnlyThreeCourseException() {
		Long userId=1L;
		List<Long> calenderIds= new ArrayList<>();
		calenderIds.add(1L);
		Course course1=Course.builder().courseId(1L).build();
		Course course2=Course.builder().courseId(1L).build();
		User user=User.builder().userId(1L).build();
		TrainingCalender calender1=new TrainingCalender(1L, course1, "Vamsha", LocalDate.parse("2024-05-15"), LocalDate.parse("2024-05-25"));
		TrainingCalender calender2=new TrainingCalender(2L, course2, "Vamsha", LocalDate.parse("2024-05-15"), LocalDate.parse("2024-05-25"));
		TrainingCalender calender3=new TrainingCalender(3L, course2, "Vamsha", LocalDate.parse("2024-05-15"), LocalDate.parse("2024-05-25"));
		TrainingCalender calender4=new TrainingCalender(4L, course2, "Vamsha", LocalDate.parse("2024-04-15"), LocalDate.parse("2024-05-25"));
		TrainingCalender calender5=new TrainingCalender(5L, course2, "Vamsha", LocalDate.parse("2024-06-15"), LocalDate.parse("2024-05-25"));
		Enrollment enrollment1=Enrollment.builder().enrollmentId(1L).user(user).trainingCalender(calender2).staus(Status.INPROGRESS).build();
		Enrollment enrollment2=Enrollment.builder().enrollmentId(1L).user(user).trainingCalender(calender3).staus(Status.INPROGRESS).build();
		Enrollment enrollment3=Enrollment.builder().enrollmentId(1L).user(user).trainingCalender(calender4).staus(Status.SCHEDULED).build();
		Enrollment enrollment4=Enrollment.builder().enrollmentId(1L).user(user).trainingCalender(calender5).staus(Status.COMPLETED).build();
		Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(user));
		Mockito.when(calenderRepository.findAllById(calenderIds)).thenReturn(Arrays.asList(calender1));
		Mockito.when(enrollmentRepository.findByUser(user)).thenReturn(Arrays.asList(enrollment1,enrollment2,enrollment3,enrollment4));
	    Mockito.when(enrollmentRepository.saveAll(anyList())).thenReturn(Arrays.asList(Enrollment.builder().enrollmentId(2L).build()));
		assertThrows(OnlyThreeCourseException.class, ()->enrollmentServiceImpl.enrollCourse(userId, calenderIds));	
	}
	
}
