package com.squad4.trainingacademy.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyList;

import java.time.LocalDate;
import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.squad4.trainingacademy.entity.Course;
import com.squad4.trainingacademy.entity.Enrollment;
import com.squad4.trainingacademy.entity.Status;
import com.squad4.trainingacademy.entity.TrainingCalender;
import com.squad4.trainingacademy.entity.User;
import com.squad4.trainingacademy.repository.EnrollmentRepository;
import com.squad4.trainingacademy.serviceimpl.SuchedulerServiceImpl;

@ExtendWith(SpringExtension.class)
class SchedulerServiceImplTest {
	
	@Mock
	EnrollmentRepository enrollmentRepository;
	
	@InjectMocks
	SuchedulerServiceImpl schedulerServiceImpl;
	
	@Test
	void testSuccess() {
		Course course2=Course.builder().courseId(1L).build();
		User user=User.builder().userId(1L).build();
	    TrainingCalender calender2=new TrainingCalender(2L, course2, "Vamsha", LocalDate.parse("2024-05-08"), LocalDate.parse("2024-05-25"));
		Enrollment enrollment1=Enrollment.builder().enrollmentId(1L).user(user).trainingCalender(calender2).staus(Status.SCHEDULED).build();
		Mockito.when(enrollmentRepository.findAll()).thenReturn(Arrays.asList(enrollment1));
		Mockito.when(enrollmentRepository.saveAll(anyList())).thenReturn(Arrays.asList(enrollment1));
		schedulerServiceImpl.updateStatus();
		assertEquals(Status.INPROGRESS, enrollment1.getStaus());
		
	}
	
	@Test
	void testSuccess2() {
		Course course2=Course.builder().courseId(1L).build();
		User user=User.builder().userId(1L).build();
	    TrainingCalender calender2=new TrainingCalender(2L, course2, "Vamsha", LocalDate.parse("2024-05-10"), LocalDate.parse("2024-05-08"));
		Enrollment enrollment1=Enrollment.builder().enrollmentId(1L).user(user).trainingCalender(calender2).staus(Status.SCHEDULED).build();
		Mockito.when(enrollmentRepository.findAll()).thenReturn(Arrays.asList(enrollment1));
		Mockito.when(enrollmentRepository.saveAll(anyList())).thenReturn(Arrays.asList(enrollment1));
		schedulerServiceImpl.updateStatus();
		assertEquals(Status.COMPLETED, enrollment1.getStaus());
		
	}
}
