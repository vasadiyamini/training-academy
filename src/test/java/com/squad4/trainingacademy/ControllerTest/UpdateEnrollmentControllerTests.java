package com.squad4.trainingacademy.ControllerTest;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.squad4.trainingacademy.controller.UpdateEnrollmentController;
import com.squad4.trainingacademy.dto.ApiResponse;
import com.squad4.trainingacademy.service.UpdateEnrollmentService;

@WebMvcTest(UpdateEnrollmentController.class)
public class UpdateEnrollmentControllerTests {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private UpdateEnrollmentService updateEnrollmentService;

	@Test
	public void testUpdateEnrollmentController() throws Exception {
		Long userId = 1L;
		Long enrollmentId = 2L;

		ApiResponse apiResponse = new ApiResponse("Success", "Ex001");

		when(updateEnrollmentService.updateenrollment(userId, enrollmentId)).thenReturn(apiResponse);

		mockMvc.perform(
				MockMvcRequestBuilders.put("/api/v1/users/{userId}/enrollments/{enrollmentId}", userId, enrollmentId))
				.andExpect(status().isOk()).andExpect(jsonPath("$.message").value("Success"))
				.andExpect(jsonPath("$.code").value(HttpStatus.OK.value()));
	}
}