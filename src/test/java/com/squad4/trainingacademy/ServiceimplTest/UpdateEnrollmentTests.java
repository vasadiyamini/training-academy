package com.squad4.trainingacademy.ServiceimplTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.squad4.trainingacademy.dto.ApiResponse;
import com.squad4.trainingacademy.entity.Enrollment;
import com.squad4.trainingacademy.entity.Status;
import com.squad4.trainingacademy.entity.User;
import com.squad4.trainingacademy.exception.EnrollmentIdNotFoundException;
import com.squad4.trainingacademy.exception.UserNotFoundException;
import com.squad4.trainingacademy.repository.EnrollmentRepository;
import com.squad4.trainingacademy.repository.UserRepository;
import com.squad4.trainingacademy.serviceimpl.UpdateEnrollmentServiceImpl;
import com.squad4.trainingacademy.util.CustomCode;

@SpringBootTest
public class UpdateEnrollmentTests {

	@MockBean
	private UserRepository userRepository;

	@MockBean
	private EnrollmentRepository enrollmentRepository;

	private UpdateEnrollmentServiceImpl updateEnrollmentService;

	@BeforeEach
	public void setUp() {
		updateEnrollmentService = new UpdateEnrollmentServiceImpl(userRepository, enrollmentRepository);
	}

	@Test
	public void testUpdateEnrollment_Success() {
		Long userId = 1L;
		Long enrollmentId = 2L;

		User user = new User();
		user.setUserId(userId);

		Enrollment enrollment = new Enrollment();
		// enrollment.setUser(enrollmentId);
		enrollment.setStaus(Status.CANCELLED);

		when(userRepository.findById(userId)).thenReturn(Optional.of(user));
		when(enrollmentRepository.findById(enrollmentId)).thenReturn(Optional.of(enrollment));
		when(enrollmentRepository.save(enrollment)).thenReturn(enrollment);

		ApiResponse expectedApiResponse = new ApiResponse(CustomCode.USER_CANCELED_ENROLLMENT_SUCCESSFULLY_MESSAGE,
				CustomCode.USER_CANCELED_ENROLLMENT_SUCCESSFULLY_CODE);

		ApiResponse actualApiResponse = updateEnrollmentService.updateenrollment(userId, enrollmentId);

		assertEquals(expectedApiResponse.message(), actualApiResponse.message());
		assertEquals(expectedApiResponse.code(), actualApiResponse.code());
	}

	@Test
	public void testUpdateEnrollment_UserNotFound() {
		Long userId = 1L;
		Long enrollmentId = 2L;

		when(userRepository.findById(userId)).thenReturn(Optional.empty());

		try {
			updateEnrollmentService.updateenrollment(userId, enrollmentId);
		} catch (UserNotFoundException e) {

			return;
		}

		throw new AssertionError("UserNotFoundException was not thrown");
	}

	@Test
	public void testUpdateEnrollment_EnrollmentNotFound() {
		Long userId = 1L;
		Long enrollmentId = 2L;

		User user = new User();
		user.setUserId(userId);

		when(userRepository.findById(userId)).thenReturn(Optional.of(user));
		when(enrollmentRepository.findById(enrollmentId)).thenReturn(Optional.empty());

		try {
			updateEnrollmentService.updateenrollment(userId, enrollmentId);
		} catch (EnrollmentIdNotFoundException e) {

			return;
		}

		throw new AssertionError("EnrollmentIdNotFoundException was not thrown");
	}
}
