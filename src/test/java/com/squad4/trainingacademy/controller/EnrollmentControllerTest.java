package com.squad4.trainingacademy.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.squad4.trainingacademy.dto.ApiResponse;
import com.squad4.trainingacademy.service.EnrollmentService;
import com.squad4.trainingacademy.util.CustomCode;

@ExtendWith(SpringExtension.class)
class EnrollmentControllerTest {
	@Mock
	EnrollmentService enrollmentService;
	@InjectMocks
	EnrollmentController enrollmentController;
	@Test
	void testSuccess() {
		Long userId=1L;
		List<Long> calenderIds= new ArrayList<>();
		calenderIds.add(1L);
		ApiResponse apiResponse=new ApiResponse(CustomCode.COURSE_ENROLLED_SUCCESSFULLY_MESSAGE, CustomCode.COURSE_ENROLLED_SUCCESSFULLY_CODE);
		Mockito.when(enrollmentService.enrollCourse(userId, calenderIds)).thenReturn(apiResponse);
		ResponseEntity<ApiResponse> responseEntity=enrollmentController.courseEnrollment(userId, calenderIds);
		assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
	}
}
