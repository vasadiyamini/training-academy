package com.squad4.trainingacademy.dto;

import lombok.Builder;




/*
 * Success message
 * success code
 */
@Builder
public record ApiResponse(String message,String code) {


}
