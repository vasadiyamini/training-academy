package com.squad4.trainingacademy.dto;

import java.util.List;

import lombok.Builder;

@Builder
public record EnrollmentDto(List<Long> calenderIds) {

}
