package com.squad4.trainingacademy.dto;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TrainingCalenderDto {
	private Long traingId;
	private String courseCode;
	private String trainerName;
	private LocalDate startDate;
	private LocalDate endDate;
}
