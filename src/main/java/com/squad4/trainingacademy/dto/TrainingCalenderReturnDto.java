package com.squad4.trainingacademy.dto;

import org.springframework.data.domain.Page;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TrainingCalenderReturnDto {
	private ApiResponse apiResponse;
	private Page<TrainingCalenderDto> response;
}
