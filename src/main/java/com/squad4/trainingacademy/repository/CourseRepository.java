package com.squad4.trainingacademy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.squad4.trainingacademy.entity.Course;

public interface CourseRepository extends JpaRepository<Course, Long> {

}
