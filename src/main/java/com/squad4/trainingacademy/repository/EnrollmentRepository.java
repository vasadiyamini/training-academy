package com.squad4.trainingacademy.repository;


import java.util.Optional;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;

import com.squad4.trainingacademy.entity.Enrollment;
import com.squad4.trainingacademy.entity.User;

public interface EnrollmentRepository extends JpaRepository<Enrollment, Long> {

	Optional<Enrollment> findById(Long enrollmentId);

	List<Enrollment> findByUser(User user);

}
/* Optional<Registration> findByEmail(String email); */