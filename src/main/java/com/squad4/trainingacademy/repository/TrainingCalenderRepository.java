package com.squad4.trainingacademy.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.squad4.trainingacademy.entity.TrainingCalender;

public interface TrainingCalenderRepository extends JpaRepository<TrainingCalender, Long> {
	List<TrainingCalender> findByStartDateAfter(LocalDate date);

	Page<TrainingCalender> findByStartDateAfter(LocalDate now, Pageable pageable);
}
