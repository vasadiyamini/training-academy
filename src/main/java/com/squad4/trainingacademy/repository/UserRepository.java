package com.squad4.trainingacademy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.squad4.trainingacademy.entity.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
