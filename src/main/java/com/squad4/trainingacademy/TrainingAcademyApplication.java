package com.squad4.trainingacademy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class TrainingAcademyApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrainingAcademyApplication.class, args);
	}

}
