package com.squad4.trainingacademy.service;

import com.squad4.trainingacademy.dto.TrainingCalenderReturnDto;

public interface TrainingCalenderService {

	TrainingCalenderReturnDto getTriningCalenderShedule(Integer pageNumber, Integer pageSize);

}
