package com.squad4.trainingacademy.service;

import com.squad4.trainingacademy.dto.ApiResponse;

public interface UpdateEnrollmentService {
	ApiResponse updateenrollment(Long userId, Long enrollmentId);
}
