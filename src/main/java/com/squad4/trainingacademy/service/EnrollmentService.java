package com.squad4.trainingacademy.service;

import java.util.List;

import com.squad4.trainingacademy.dto.ApiResponse;

public interface EnrollmentService {
	/**
	 * 
	 * @param userId        userId for enrollment
	 * @param calenderIds   calender id of training
	 * @return apiresponse  success message and code
	 */
	ApiResponse enrollCourse(Long userId,List<Long> calenderIds);
}
