package com.squad4.trainingacademy.serviceimpl;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.squad4.trainingacademy.dto.ApiResponse;
import com.squad4.trainingacademy.dto.TrainingCalenderDto;
import com.squad4.trainingacademy.dto.TrainingCalenderReturnDto;
import com.squad4.trainingacademy.entity.TrainingCalender;
import com.squad4.trainingacademy.exception.NoTrainingException;
import com.squad4.trainingacademy.exception.ResultNotFoundInCurrentPageException;
import com.squad4.trainingacademy.repository.TrainingCalenderRepository;
import com.squad4.trainingacademy.service.TrainingCalenderService;
import com.squad4.trainingacademy.util.CustomCode;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@AllArgsConstructor
@Slf4j
public class TrainingCalenderImpl implements TrainingCalenderService {
	private final TrainingCalenderRepository trainingCalenderRepository;

	/**
	 * @param pageNumber taking from the user 
	 * @param pageSize taking from the user
	 * @return will give list of records
	 */
	@Override
	public TrainingCalenderReturnDto getTriningCalenderShedule(Integer pageNumber, Integer pageSize) {
		Pageable pageable = PageRequest.of(pageNumber, pageSize);
		Page<TrainingCalender> trainings = trainingCalenderRepository.findByStartDateAfter(LocalDate.now(), pageable);
		List<TrainingCalender> trainingCalender = trainingCalenderRepository.findByStartDateAfter(LocalDate.now());
		log.warn("no trainings are exist in data base");
		if (trainingCalender.isEmpty()) {
			throw new NoTrainingException();
		}
		log.warn("page not there");
		if (!trainings.hasContent()) {
			throw new ResultNotFoundInCurrentPageException();
		}
		List<TrainingCalenderDto> trainingCalenderDto = trainings.getContent().stream().map(request -> {
			TrainingCalenderDto training = new TrainingCalenderDto();
			BeanUtils.copyProperties(request, training);
			training.setTraingId(request.getCalendeId());
			training.setCourseCode(request.getCourse().getCourseCode());
			return training;
		}).toList();
		log.info("List of arrays  return successfully");
		return TrainingCalenderReturnDto.builder()
				.apiResponse(ApiResponse.builder().code(CustomCode.CALENDER_FETCHED_SUCCESSFULLY_CODE)
						.message(CustomCode.CALENDER_FETCHED_SUCCESSFULLY_MESSAGE).build())
				.response(new PageImpl<>(trainingCalenderDto)).build();

	}

}
