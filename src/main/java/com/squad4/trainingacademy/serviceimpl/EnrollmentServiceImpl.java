package com.squad4.trainingacademy.serviceimpl;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.squad4.trainingacademy.dto.ApiResponse;
import com.squad4.trainingacademy.entity.Enrollment;
import com.squad4.trainingacademy.entity.Status;
import com.squad4.trainingacademy.entity.TrainingCalender;
import com.squad4.trainingacademy.entity.User;
import com.squad4.trainingacademy.exception.CalenderIdNotFoundException;
import com.squad4.trainingacademy.exception.CourseAlreadyRequestedException;
import com.squad4.trainingacademy.exception.InvalidDateException;
import com.squad4.trainingacademy.exception.OnlyThreeCourseException;
import com.squad4.trainingacademy.exception.UserNotFoundException;
import com.squad4.trainingacademy.repository.EnrollmentRepository;
import com.squad4.trainingacademy.repository.TrainingCalenderRepository;
import com.squad4.trainingacademy.repository.UserRepository;
import com.squad4.trainingacademy.service.EnrollmentService;
import com.squad4.trainingacademy.util.CustomCode;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class EnrollmentServiceImpl implements EnrollmentService {
	
	private final UserRepository userRepository;
	private final TrainingCalenderRepository calenderRepository;
	private final EnrollmentRepository enrollmentRepository;
	/**
	 * Enrollment of Courses
	 */
	@Override
	public ApiResponse enrollCourse(Long userId,List<Long> calenderIds) {
		User user=userRepository.findById(userId).orElseThrow(UserNotFoundException::new);
		
		List<TrainingCalender> trainingCalenders=calenderRepository.findAllById(calenderIds);
		
		if(trainingCalenders.isEmpty()) {
			log.warn(CustomCode.COURSE_NOTFOUND_EXCEPTION_MESSAGE);
			throw new CalenderIdNotFoundException();
		}
		
		trainingCalenders.forEach(request->{
			LocalDate courseStartDate=request.getStartDate();
			if(courseStartDate.equals(LocalDate.now()) || courseStartDate.isBefore(LocalDate.now()) || courseStartDate.equals(LocalDate.now().plusDays(1)) || courseStartDate.equals(LocalDate.now().plusDays(2)) || courseStartDate.isAfter(LocalDate.now().plusDays(20))) { 
				log.warn(CustomCode.INVALID_DATE_EXCEPTION_MESSAGE);
				throw new InvalidDateException();
			}
			
		});
		
		List<Enrollment> enrollments=enrollmentRepository.findByUser(user);
		int numberOfRequest=calenderIds.size();
		
		List<Enrollment> presentenrollment=enrollments.stream().filter(enrollment->enrollment.getStaus().equals(Status.INPROGRESS)||enrollment.getStaus().equals(Status.SCHEDULED)).toList();
		int count=presentenrollment.size();
		if(count==3 || numberOfRequest+count>3) {
			log.warn(CustomCode.ONLY_THREE_COURSE_ENROLLED_EXCEPTION_MESSAGE);
			throw new OnlyThreeCourseException();
		}
		
		List<TrainingCalender> existingTrainingCalenders=presentenrollment.stream().map(Enrollment::getTrainingCalender).toList();
	    Map<Long,TrainingCalender> existingCalenderMap = existingTrainingCalenders.stream().collect(Collectors.toMap(TrainingCalender::getCalendeId,Function.identity()));
		
	    trainingCalenders.forEach(request->{
	    	if(Objects.nonNull(existingCalenderMap.get(request.getCalendeId()))) {
	    		    log.warn(CustomCode.ALREADY_COURSE_ENROLLED_EXCEPTION_MESSAGE);
	    			throw new CourseAlreadyRequestedException();
	    	}
	    });
	    
	    enrollmentRepository.saveAll(trainingCalenders.stream().map(calender->
		  Enrollment.builder().user(user).trainingCalender(calender).staus(Status.SCHEDULED).build() 
	   ).toList());
		
		
		return new ApiResponse(CustomCode.COURSE_ENROLLED_SUCCESSFULLY_MESSAGE, CustomCode.COURSE_ENROLLED_SUCCESSFULLY_CODE);
	}

}
