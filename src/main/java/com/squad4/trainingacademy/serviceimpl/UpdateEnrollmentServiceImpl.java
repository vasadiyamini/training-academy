package com.squad4.trainingacademy.serviceimpl;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.squad4.trainingacademy.dto.ApiResponse;
import com.squad4.trainingacademy.entity.Enrollment;
import com.squad4.trainingacademy.entity.Status;
import com.squad4.trainingacademy.entity.User;
import com.squad4.trainingacademy.exception.EnrollmentIdNotFoundException;
import com.squad4.trainingacademy.exception.UserNotFoundException;
import com.squad4.trainingacademy.repository.EnrollmentRepository;
import com.squad4.trainingacademy.repository.UserRepository;
import com.squad4.trainingacademy.service.UpdateEnrollmentService;
import com.squad4.trainingacademy.util.CustomCode;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UpdateEnrollmentServiceImpl implements UpdateEnrollmentService {
	private final UserRepository userRepository;
	private final EnrollmentRepository enrollmentRepository;

	@Override
	public ApiResponse updateenrollment(Long userId, Long enrollmentId) {
		User user = userRepository.findById(userId).orElseThrow(UserNotFoundException::new);
		Optional<Enrollment> enroll = enrollmentRepository.findById(enrollmentId);

		if (enroll.isPresent()) {
			enroll.get().setStaus(Status.CANCELLED);
			enrollmentRepository.save(enroll.get());
			ApiResponse api = new ApiResponse(CustomCode.USER_CANCELED_ENROLLMENT_SUCCESSFULLY_MESSAGE,
					CustomCode.USER_CANCELED_ENROLLMENT_SUCCESSFULLY_CODE);
			return api;
		}
		throw new EnrollmentIdNotFoundException();
	}
}