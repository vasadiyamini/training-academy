package com.squad4.trainingacademy.serviceimpl;

import java.time.LocalDate;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.squad4.trainingacademy.entity.Status;
import com.squad4.trainingacademy.repository.EnrollmentRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class SuchedulerServiceImpl {
	/**
	 * update status based on date
	 */
	private final EnrollmentRepository enrollmentRepository;
	
	@Scheduled(fixedRate  = 6000)
	public void updateStatus() {

		enrollmentRepository.saveAll(enrollmentRepository.findAll().stream().map(enrollment->
		{
			if(enrollment.getTrainingCalender().getStartDate().equals(LocalDate.now())) {
				log.info("Course Started today");
				enrollment.setStaus(Status.INPROGRESS);
			}
			if(enrollment.getTrainingCalender().getEndDate().equals(LocalDate.now())) {
				log.info("Course Completed today");
				enrollment.setStaus(Status.COMPLETED);
			}
			return enrollment;
		}).toList());
	}

}
