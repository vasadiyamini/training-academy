package com.squad4.trainingacademy.util;

public class CustomCode {

	/**
	 * Successful Code And Message
	 */

	public static final String CALENDER_FETCHED_SUCCESSFULLY_MESSAGE = "Calender shcedules fetched successfully";
	public static final String CALENDER_FETCHED_SUCCESSFULLY_CODE = "SUCCESS001";
	public static final String COURSE_ENROLLED_SUCCESSFULLY_MESSAGE = "Course Enrolled Successfully";
	public static final String COURSE_ENROLLED_SUCCESSFULLY_CODE = "SUCCESS002";
	public static final String USER_CANCELED_ENROLLMENT_SUCCESSFULLY_MESSAGE = "User cancelled enrollment Successfully";
	public static final String USER_CANCELED_ENROLLMENT_SUCCESSFULLY_CODE = "SUCCESS003";

	/**
	 * Exception Code
	 */
	public static final String USER_NOTFOUND_EXCEPTION_MESSAGE = "User not exist";
	public static final String USER_NOTFOUND_EXCEPTIONEXCEPTION_CODE = "EX001";
	public static final String COURSE_NOTFOUND_EXCEPTION_MESSAGE = "Course Not Exist";
	public static final String COURSE_NOTFOUND_EXCEPTIONEXCEPTION_CODE = "EX002";
	public static final String ALREADY_COURSE_ENROLLED_EXCEPTION_MESSAGE = "User Alredy Enrolled For this course";
	public static final String ALREADY_COURSE_ENROLLED_EXCEPTION_CODE = "EX003";
	public static final String ONLY_THREE_COURSE_ENROLLED_EXCEPTION_MESSAGE = "user enrolled for three courses";
	public static final String ONLY_THREE_COURSE_ENROLLED_EXCEPTION_CODE = "EX004";
	public static final String INVALID_DATE_EXCEPTION_MESSAGE = "Invalid date";
	public static final String INVALID_DATE_EXCEPTION_CODE = "EX005";
	public static final String ENROLLMENT_NOTFOUND_EXCEPTION_MESSAGE = "Enrollment not found ";
	public static final String ENROLLMENT_NOTFOUND_EXCEPTION_CODE = "EX006";
	public static final String COURSE_INPROGRESS_EXCEPTION_MESSAGE = "This course alredy inprogress";
	public static final String COURSE_INPROGRESS_EXCEPTION_CODE = "EX007";
	public static final String COURSE_ALREDY_COMPETED_EXCEPTION_MESSAGE = "This course alredy completed";
	public static final String COURSE_ALREDY_COMPETED_EXCEPTION_CODE = "EX008";
	public static final String NO_TRAINING_EXCEPTION_MESSAGE = "Currently No Training was on Going";
	public static final String NO_TRAINING_EXCEPTION_CODE = "EX009";
	public static final String RESULT_NOTFOUND_INCURRENT_PAGE_EXCEPTION_MESSAGE = "There are no results in Current page";
	public static final String RESULT_NOTFOUND_INCURRENT_PAGE_EXCEPTION_CODE = "EX0010";
	private CustomCode() {
		super();
	}
}
