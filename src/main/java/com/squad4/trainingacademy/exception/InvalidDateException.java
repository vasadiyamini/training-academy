package com.squad4.trainingacademy.exception;

import com.squad4.trainingacademy.util.CustomCode;

public class InvalidDateException extends GlobalException{
	 
	
	 
		/**
		 *
		 */
		private static final long serialVersionUID = 1L;
		
		public InvalidDateException() {
			super(CustomCode.INVALID_DATE_EXCEPTION_MESSAGE, CustomCode.INVALID_DATE_EXCEPTION_MESSAGE);
		}
		
	 
	}
	 