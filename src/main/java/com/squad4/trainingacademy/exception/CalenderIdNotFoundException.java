package com.squad4.trainingacademy.exception;

import com.squad4.trainingacademy.util.CustomCode;

public class CalenderIdNotFoundException extends GlobalException{
	 
	
	 
		/**
		 *
		 */
		private static final long serialVersionUID = 1L;
		
		public CalenderIdNotFoundException() {
			super(CustomCode.COURSE_NOTFOUND_EXCEPTION_MESSAGE, CustomCode.COURSE_NOTFOUND_EXCEPTIONEXCEPTION_CODE);
		}
		
	 
	}
	 