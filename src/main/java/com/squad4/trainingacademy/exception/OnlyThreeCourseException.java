package com.squad4.trainingacademy.exception;

import com.squad4.trainingacademy.util.CustomCode;

public class OnlyThreeCourseException extends GlobalException{
	 
	
	 
		/**
		 *
		 */
		private static final long serialVersionUID = 1L;
		
		public OnlyThreeCourseException() {
			super(CustomCode.ONLY_THREE_COURSE_ENROLLED_EXCEPTION_MESSAGE, CustomCode.ONLY_THREE_COURSE_ENROLLED_EXCEPTION_CODE);
		}
		
	 
	}
	 