package com.squad4.trainingacademy.exception;

import com.squad4.trainingacademy.util.CustomCode;

public class ResultNotFoundInCurrentPageException extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ResultNotFoundInCurrentPageException() {
		super(CustomCode.RESULT_NOTFOUND_INCURRENT_PAGE_EXCEPTION_CODE,
				CustomCode.RESULT_NOTFOUND_INCURRENT_PAGE_EXCEPTION_MESSAGE);

	}

}
