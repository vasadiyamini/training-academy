package com.squad4.trainingacademy.exception;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatusCode status, WebRequest request) {
		Map<String, String> errors = new HashMap<>();
		ex.getBindingResult().getAllErrors().forEach(error -> {
			String fieldName = ((FieldError) error).getField();
			String message = error.getDefaultMessage();
			errors.put(fieldName, message);
		});
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errors);
	}
	@ExceptionHandler(NoTrainingException.class)
	protected ResponseEntity<ErrorResponse> handleNoTrainingException(
			NoTrainingException bookMyMovieGlobalException) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ErrorResponse.builder()
				.code(bookMyMovieGlobalException.getCode()).message(bookMyMovieGlobalException.getMessage()).build());
	}
	
	@ExceptionHandler(ResultNotFoundInCurrentPageException.class)
	protected ResponseEntity<ErrorResponse> handleResultNotFoundInCurrentPageException(
			ResultNotFoundInCurrentPageException bookMyMovieGlobalException) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ErrorResponse.builder()
				.code(bookMyMovieGlobalException.getCode()).message(bookMyMovieGlobalException.getMessage()).build());
=======
	
	@ExceptionHandler(UserNotFoundException.class)
	protected ResponseEntity<ErrorResponse> handleUserNotFoundException(UserNotFoundException bookMyMovieGlobalException) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.body(ErrorResponse.builder().code(bookMyMovieGlobalException.getCode())
						.message(bookMyMovieGlobalException.getMessage()).build());
	}
	
	@ExceptionHandler(CalenderIdNotFoundException.class)
	protected ResponseEntity<ErrorResponse> handleCalenderIdNotFoundException(CalenderIdNotFoundException bookMyMovieGlobalException) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
				.body(ErrorResponse.builder().code(bookMyMovieGlobalException.getCode())
						.message(bookMyMovieGlobalException.getMessage()).build());
	}
	
	@ExceptionHandler(OnlyThreeCourseException.class)
	protected ResponseEntity<ErrorResponse> handleOnlyThreeCourseException(OnlyThreeCourseException bookMyMovieGlobalException) {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
				.body(ErrorResponse.builder().code(bookMyMovieGlobalException.getCode())
						.message(bookMyMovieGlobalException.getMessage()).build());
	}
	
	@ExceptionHandler(CourseAlreadyRequestedException.class)
	protected ResponseEntity<ErrorResponse> handleCourseAlreadyRequestedException(CourseAlreadyRequestedException bookMyMovieGlobalException) {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
				.body(ErrorResponse.builder().code(bookMyMovieGlobalException.getCode())
						.message(bookMyMovieGlobalException.getMessage()).build());
	}
	
	@ExceptionHandler(InvalidDateException.class)
	protected ResponseEntity<ErrorResponse> handleInvalidDateException(InvalidDateException bookMyMovieGlobalException) {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
				.body(ErrorResponse.builder().code(bookMyMovieGlobalException.getCode())
						.message(bookMyMovieGlobalException.getMessage()).build());
	}

	@ExceptionHandler(UserNotFoundException.class)
	protected ResponseEntity<ErrorResponse> handleUserNotFoundException(
			UserNotFoundException bookMyMovieGlobalException) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ErrorResponse.builder()
				.code(bookMyMovieGlobalException.getCode()).message(bookMyMovieGlobalException.getMessage()).build());
	}

	@ExceptionHandler(EnrollmentIdNotFoundException.class)
	protected ResponseEntity<ErrorResponse> handleEnrollmentIdNotFoundException(
			EnrollmentIdNotFoundException bookMyMovieGlobalException) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ErrorResponse.builder()
				.code(bookMyMovieGlobalException.getCode()).message(bookMyMovieGlobalException.getMessage()).build());
	}

}
