package com.squad4.trainingacademy.exception;

import com.squad4.trainingacademy.util.CustomCode;


public class UserNotFoundException extends GlobalException {

	private static final long serialVersionUID = 1L;

	public UserNotFoundException() {
		super(CustomCode.USER_NOTFOUND_EXCEPTION_MESSAGE, CustomCode.USER_NOTFOUND_EXCEPTIONEXCEPTION_CODE);

	}

	/**
	 * 
	 */

}
