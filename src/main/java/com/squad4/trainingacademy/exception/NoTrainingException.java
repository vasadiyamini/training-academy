package com.squad4.trainingacademy.exception;

import com.squad4.trainingacademy.util.CustomCode;

public class NoTrainingException extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NoTrainingException() {
		super(CustomCode.NO_TRAINING_EXCEPTION_MESSAGE, CustomCode.NO_TRAINING_EXCEPTION_CODE);
	}

}
