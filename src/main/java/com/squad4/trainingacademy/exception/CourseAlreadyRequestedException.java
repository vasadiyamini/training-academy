package com.squad4.trainingacademy.exception;

import com.squad4.trainingacademy.util.CustomCode;

public class CourseAlreadyRequestedException extends GlobalException{
	 
	
	 
		/**
		 *
		 */
		private static final long serialVersionUID = 1L;
		
		public CourseAlreadyRequestedException() {
			super(CustomCode.ALREADY_COURSE_ENROLLED_EXCEPTION_MESSAGE, CustomCode.ALREADY_COURSE_ENROLLED_EXCEPTION_CODE);
		}
		
	 
	}
	 