package com.squad4.trainingacademy.exception;

import com.squad4.trainingacademy.util.CustomCode;

public class EnrollmentIdNotFoundException extends GlobalException {

	private static final long serialVersionUID = 1L;

	public EnrollmentIdNotFoundException() {
		super(CustomCode.ENROLLMENT_NOTFOUND_EXCEPTION_MESSAGE, CustomCode.ENROLLMENT_NOTFOUND_EXCEPTION_CODE);

	}

}
