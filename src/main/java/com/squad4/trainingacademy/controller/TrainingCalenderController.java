package com.squad4.trainingacademy.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.squad4.trainingacademy.dto.TrainingCalenderReturnDto;
import com.squad4.trainingacademy.service.TrainingCalenderService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
@Slf4j
public class TrainingCalenderController {
	private final TrainingCalenderService trainingCalenderService;
/**
 * 
 * @param pageNumber taking from the user
 * @param pageSize taking from the user
 * @return will give list of records
 */
	@GetMapping("/trainingcalender")
	ResponseEntity<TrainingCalenderReturnDto> getTriningCalender(
			@RequestParam(required = false, defaultValue = "0") Integer pageNumber,
			@RequestParam(required = false, defaultValue = "1") Integer pageSize) {
		log.info("successfully return the calender details");
		return ResponseEntity.status(HttpStatus.OK)
				.body(trainingCalenderService.getTriningCalenderShedule(pageNumber, pageSize));
	}

}
