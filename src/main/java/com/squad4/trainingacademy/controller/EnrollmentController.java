package com.squad4.trainingacademy.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.squad4.trainingacademy.dto.ApiResponse;
import com.squad4.trainingacademy.service.EnrollmentService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class EnrollmentController {
	
	private final EnrollmentService enrollmentService;
	
	/**
	 * 
	 * @param userId           user id for enrollment
	 * @param calenderId       training id for enrollment
	 * @return apiresponse     success message and code
	 */
	@PostMapping("/users/{userId}/enrollments")
	ResponseEntity<ApiResponse> courseEnrollment(@PathVariable("userId") Long userId,@RequestBody List<Long> calenderId) {
		return new ResponseEntity<>(enrollmentService.enrollCourse(userId, calenderId),HttpStatus.CREATED);
	}
}
