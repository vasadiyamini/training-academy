package com.squad4.trainingacademy.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.squad4.trainingacademy.dto.ApiResponse;
import com.squad4.trainingacademy.service.UpdateEnrollmentService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
@Slf4j

public class UpdateEnrollmentController {

	private final UpdateEnrollmentService updateEnrollmentService;

	@PutMapping("/users/{userId}/enrollments/{enrollmentId}")
	public ResponseEntity<ApiResponse> updates(@PathVariable Long userId, @PathVariable Long enrollmentId) {
		return ResponseEntity.status(HttpStatus.CREATED)
				.body(updateEnrollmentService.updateenrollment(userId, enrollmentId));

	}
}
