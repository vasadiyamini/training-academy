package com.squad4.trainingacademy.entity;

import java.time.LocalDate;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class TrainingCalender {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long calendeId;
	@ManyToOne(cascade = CascadeType.ALL)
	private Course course;
	private String trainerName;
	private LocalDate startDate;
	private LocalDate endDate;
}
