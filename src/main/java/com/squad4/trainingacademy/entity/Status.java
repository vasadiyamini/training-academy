package com.squad4.trainingacademy.entity;

public enum Status {
	SCHEDULED, INPROGRESS, CANCELLED, COMPLETED;
}
